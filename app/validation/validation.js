function kiemTraKhongDuocDeTrong(value, idErr) {
  var value = value.trim();
  var a = document.getElementById(idErr);
  if (value == "") {
    return (
      (a.innerHTML = `Không được để trống`), (a.style.display = "block"), false
    );
  } else {
    return (a.innerHTML = ``), true;
  }
}

function kiemTraChon(value, idErr) {
  var value = value.trim();
  var a = document.getElementById(idErr);
  if (value == "0") {
    return (a.innerHTML = `Phải chọn`), (a.style.display = "block"), false;
  } else {
    return (a.innerHTML = ``), true;
  }
}

function kiemTraTrung(idNV, nhanVienArr) {
  // console.log(idNV, nhanVienArr);
  // /findIndex return vị trí của item nếu điều kiện true, nếu không tìm thấy trả về -1
  var id = nhanVienArr.findIndex(function (item) {
    console.log(item.taiKhoan);
    return item.taiKhoan == idNV;
  });
  console.log(id);
  var a = document.getElementById("tpTaiKhoan");
  if (id != -1) {
    return (
      (a.innerHTML = "Tài khoản nhân viên đã tồn tại"),
      (a.style.display = "block"),
      false
    );
  } else {
    return (a.innerHTML = ""), true;
  }
}

function kiemTraChu(value) {
  var re =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  var isNumber = re.test(value);
  var a = document.getElementById("tpHoTen");
  if (isNumber) {
    return (a.innerHTML = ``), true;
  } else {
    return (
      (a.innerHTML = `Tên nhân viên phải là chữ`),
      (a.style.display = "block"),
      false
    );
  }
}

function kiemTraMatKhau(value) {
  const re =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;
  var isPass = re.test(value);
  var a = document.getElementById("tpMatKhau");
  if (isPass) {
    return (a.innerText = ``), true;
  } else {
    return (
      (a.innerText = `Mật Khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`),
      (a.style.display = "block"),
      false
    );
  }
}

function kiemTraEmail(value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(value);
  var a = document.getElementById("tpEmail");
  if (isEmail) {
    return (a.innerText = ``), true;
  } else {
    return (
      (a.innerText = ` Email phải đúng định dạng`),
      (a.style.display = "block"),
      false
    );
  }
}

function kiemTraDoDai(value, idErr, max) {
  var length = value.length;
  console.log(length);
  var a = document.getElementById(idErr);
  if (length >= max) {
    return (
      (a.innerText = `Không vượt quá ${max} ký tự`),
      (document.getElementById("tpMoTa").style.display = "block"),
      false
    );
  } else {
    return (a.innerText = ``), true;
  }
}
