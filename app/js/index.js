const BASE_URL = "https://63bea7f8e348cb07621499d9.mockapi.io";
var DSND = [];

function fetchQLNDList() {
  batLoading();
  axios({
    url: `${BASE_URL}/QLND`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      renderQLNDList(res.data);
      DSND.push(...res.data);
      // console.log(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}

fetchQLNDList();

console.log(DSND);
function validation() {
  var QLND = layThongTinTuFrom();
  var isValid = true;
  isValid =
    kiemTraKhongDuocDeTrong(QLND.taiKhoan, "tpTaiKhoan") &&
    kiemTraTrung(QLND.taiKhoan, DSND);
  isValid =
    kiemTraKhongDuocDeTrong(QLND.hoTen, "tpHoTen") && kiemTraChu(QLND.hoTen);
  isValid =
    kiemTraKhongDuocDeTrong(QLND.matKhau, "tpMatKhau") &&
    kiemTraMatKhau(QLND.matKhau);
  isValid =
    kiemTraKhongDuocDeTrong(QLND.email, "tpEmail") && kiemTraEmail(QLND.email);
  isValid = kiemTraKhongDuocDeTrong(QLND.hinhAnh, "tpHinhAnh");
  isValid = kiemTraChon(QLND.loaiND, "tploaiNguoiDung");
  isValid = kiemTraChon(QLND.ngonNgu, "tploaiNgonNgu");
  isValid =
    kiemTraKhongDuocDeTrong(QLND.moTa, "tpMoTa") &&
    kiemTraDoDai(QLND.moTa, "tpMoTa", 60);
  return isValid;
}

function themNguoiDung() {
  var isValid = validation();
  // console.log(isValid);
  if (isValid) {
    batLoading();
    axios({
      url: `${BASE_URL}/QLND`,
      method: "POST",
      data: layThongTinTuFrom(),
    })
      .then((res) => {
        tatLoading();

        fetchQLNDList();
      })
      .catch((err) => {
        tatLoading();
      });
  } else {
    return;
  }
}

function xoaNguoiDung(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/QLND/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      fetchQLNDList();
    })
    .catch((err) => {
      tatLoading;
    });
}

function suaNguoiDung(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/QLND/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      console.log(res.data);
      showThongTinLenFrom(res.data);
    })
    .catch((err) => {
      tatLoading();
    });
}

function capNhatNguoiDung() {
  let data = layThongTinTuFrom();
  var isValid = validation();
  if (isValid) {
    axios({
      url: `${BASE_URL}/QLND/${data.id}`,
      method: "PUT",
      data: data,
    })
      .then((res) => {
        tatLoading();
        fetchQLNDList();
      })
      .catch((err) => {
        tatLoading();
      });
  }
}

// dsdasdasdwa
